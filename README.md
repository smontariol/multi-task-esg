# Multi-Task Learning for Features Extraction in Financial Annual Reports

## Getting started

All scripts are in data_scripts folder.  
1) Scraping using dowload_corpus.py  
2) PDF to txt using pdf2text.py and pdfminer tool  
3) Filter and clean sentences using clean_reports.py  


## Classification experiments:

Use classifier.py  
See all command-line arguments for details.

All scripts: (see detailed comments in the scripts)
See bash_scripts/run_multitask_seed_gpu_x.sh

Run on all 5 seeds using bash_scripts/run_all_scripts.sh  
It runs in background and prints the outpus in log files.

Use ps -ef | grep -i script  
to find them an kill them if needed.


## Predictions and analysis:  

1) Multi-task for the 5 textual features: predict_on_corpus_multitask.py  

2) ESG-related analysis using qualitative_analysis_text+numerical_ESG.ipynb