#!/bin/bash

source /home/syriellem/directory_env/formica/bin/activate
set -x 
# all tasks mono and all tasks multi with weights
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -mt -tw

# all tasks together with each task as "explicit" MLT.
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -sm -mt -tce 0
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -sm -mt -tce 1
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -sm -mt -tce 2
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -sm -mt -tce 3
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -sm -mt -tce 4

# All multi-task combinations without weights, with sequential training one one final task. 
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 1
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 2
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 3
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 1
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 2
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 3
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 1 2
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 1 3
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 1 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 2 3
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 2 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 3 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 1 2
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 1 3
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 1 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 2 3
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 2 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 3 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 1 2 3
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 1 2 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 1 3 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 2 3 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 1 2 3
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 1 2 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 1 3 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 2 3 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 1 2 3 4
CUDA_VISIBLE_DEVICES="1" python ./classifier.py -t part1_new -ep 5 -s --seed 2 -seq -mt -tc 0 1 2 3 4
