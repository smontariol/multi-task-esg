import itertools

# multi task combinations - order:
# ['Relevance[business related text / general text]',\
#        'Financial sentiment[positive/neutral/negative]',\
#        'Objectivity[objective/subjective]', 'Forward looking[FLS/non-FLS]',\
#        'ESG[ESG related/not ESG related]']

# we try all combinations for each task, then we will get the best combination per task.

# add -s to save results and --seed $seed and -seq for sequential training (mt evaluation is done too)
# do the same with weights: -tw (but without -seq)
# do this on several gpus: create one file per seed

epochs = 5
ls = ['0', '1', '2', '3', '4']

for seed in range(5):
    gpu = seed
    if seed==3:
        gpu=5
    with open(f'bash_scripts/run_multitask_seed_gpu_{str(seed)}.sh', 'w+') as f:
        f.write(
f'''#!/bin/bash

source /home/syriellem/directory_env/formica/bin/activate
set -x 
# all tasks mono and all tasks multi with weights
CUDA_VISIBLE_DEVICES="{str(gpu)}" python ./classifier.py -t part1_new -ep {str(epochs)} -s --seed {str(seed+1)}
CUDA_VISIBLE_DEVICES="{str(gpu)}" python ./classifier.py -t part1_new -ep {str(epochs)} -s --seed {str(seed+1)} -mt -tw

# all tasks together with each task as "explicit" MLT.
'''
        )

        prefix_seq = f'CUDA_VISIBLE_DEVICES="{str(gpu)}" python ./classifier.py -t part1_new -ep {str(epochs)} -s --seed {str(seed+1)} -seq -mt -tc '
        prefix_exp = f'CUDA_VISIBLE_DEVICES="{str(gpu)}" python ./classifier.py -t part1_new -ep {str(epochs)} -s --seed {str(seed+1)} -sm -mt -tce '
        
        for L in ls:
            f.write(prefix_exp + L + '\n')
        f.write('\n')
        
        f.write('# All multi-task combinations without weights, with sequential training one one final task. \n')
        for L in range(0, len(ls)+1):
            for subset in itertools.combinations(ls, L):
                if subset: # remove empty set
                    f.write(prefix_seq + ' '.join(subset) + '\n')
        
