#!/bin/bash

for seed in 0 1 2 3 4 
do
    bash bash_scripts/run_multitask_seed_gpu_$seed.sh &> bash_scripts/log_run_$seed.txt &
done