import torch
import pandas as pd
import numpy as np
import random
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import cross_val_score, StratifiedKFold
import eli5
from sklearn.svm import SVC

def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)

def tfidf_vectorize(train, val):
    tfidf_vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.8, min_df = 0.01, stop_words='english', ngram_range=(1, 2), lowercase=False)
    tfidf_vectorizer.fit(train['text'])
    print('number of features:', len(tfidf_vectorizer.get_feature_names_out()))
    train_tfidf = tfidf_vectorizer.transform(train['text'])
    print(train_tfidf.shape)
    val_tfidf = tfidf_vectorizer.transform(val['text'])
    return tfidf_vectorizer, train_tfidf, val_tfidf

def logistic_regression(train_tfidf, val_tfidf, train, tfidf_vectorizer, args):
    logreg = LogisticRegression(solver='lbfgs', max_iter=1000, random_state=args.seed)
    skf = StratifiedKFold(n_splits=5, shuffle=True, random_state=args.seed)
    skf.get_n_splits(train_tfidf)
    cv_results = cross_val_score(logreg, train_tfidf, train['label'], cv=skf, scoring='f1_macro')
    print(cv_results, cv_results.mean())

    logreg.fit(train_tfidf, train['label'])
    val_pred = logreg.predict(val_tfidf)

    w = eli5.show_weights(estimator=logreg, 
                  feature_names= list(tfidf_vectorizer.get_feature_names_out()),
                 top=(50, 5))
    w_result = pd.read_html(w.data)[0]
    #print(w_result)
    return val_pred

def svm(train_tfidf, val_tfidf, train, args):
     # Set the parameters by cross-validation
    skf = StratifiedKFold(n_splits=5, shuffle=True, random_state=args.seed)
    skf.get_n_splits(train_tfidf)
    if args.gridsearch:
        tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-2, 1e-3, 1e-4, 1e-5],
                            'C': [0.001, 0.10, 0.1, 10, 25, 50, 100]},
                            {'kernel': ['sigmoid'], 'gamma': [1e-2, 1e-3, 1e-4, 1e-5],
                            'C': [0.001, 0.10, 0.1, 10, 25, 50, 100]},
                            {'kernel': ['linear'], 'C': [0.001, 0.10, 0.1, 10, 25, 50, 100]} # add 1000
                        ]
        svm_clf = GridSearchCV(SVC(C=1), tuned_parameters, cv=skf, scoring='f1_macro', verbose=10, n_jobs=10)
        svm_clf.fit(train_tfidf, train['label'])
        print(svm_clf.best_params_, svm_clf.best_score_)
        
        best_svm = SVC(**svm_clf.best_params_)
    else:
        best_svm = SVC(C= 100, kernel= 'linear')
        # class_weight=Counter(train['label']) # doesn't improve results
    
    best_svm.fit(train_tfidf, train['label'])
    val_pred = best_svm.predict(val_tfidf)
    return val_pred
