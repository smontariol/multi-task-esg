import torch
from torch.utils.data import TensorDataset
from sklearn.model_selection import train_test_split

def prepare_data_tars(dataframe, tokenizer, cols, max_len, batch_size):
    # create datasets dict
    data = []
    labels = []
    for index, row in dataframe.iterrows():
        for key, value in cols.items():
            for v in value:
                data.append(str(v) + " [SEP] " + row['text'])
                #print(str(v) + " [SEP] " + row['text'])
                if row[key] == v:
                    labels.append(1)
                else:
                    labels.append(0)
    dataset = prepare_dataset(data, labels, tokenizer, max_len, batch_size)
    return dataset


def load_data_multitask_tars(df, target_cols_list, args):
    df = df[target_cols_list + ['Sentences']].rename(columns={'Sentences': 'text'})
    # fill missing values and map labels to ints
    map_dicts = {}
    train, test = train_test_split(df, test_size=0.2, random_state=args.seed)
    train, val = train_test_split(train, test_size=0.2, random_state=args.seed)
    return [train, val, test], map_dicts


def prepare_dataset(data, labels, tokenizer, max_len, batch_size):

    tokenized_sentences = [tokenizer.tokenize(sentence) for sentence in data]
    truncated_sentences = [sentence[:(max_len - 2)] for sentence in tokenized_sentences]
    truncated_sentences = [["[CLS]"] + sentence + ["[SEP]"] for sentence in truncated_sentences]

    input_ids = [tokenizer.convert_tokens_to_ids(sentence) for sentence in truncated_sentences]
    input_ids_padded = []
    for i in input_ids:
        while len(i) < max_len:
            i.append(0)
        input_ids_padded.append(i)


    # attention masks
    attention_masks = []
    for seq in input_ids:
        seq_mask = [float(i > 0) for i in seq]
        attention_masks.append(seq_mask)

    input_ids = torch.tensor(input_ids_padded)
    labels = torch.tensor(labels)
    attention_masks = torch.tensor(attention_masks)

    transformed_data = TensorDataset(input_ids, attention_masks, labels)

    return transformed_data
