import torch
from transformers.modeling_outputs import SequenceClassifierOutput
from transformers import RobertaForSequenceClassification, RobertaModel
from transformers.utils import ModelOutput
from datasets import load_metric
import operator
import numpy as np
from torch import nn
from torch.nn import BCEWithLogitsLoss, CrossEntropyLoss
from typing import List, Optional, Tuple, Union, Dict
import torch.nn.functional as f

metric = load_metric("f1")

def compute_f1(logits, labels):
    if logits.shape[-1] > 1:
        predictions = np.argmax(logits, axis=-1)
    else:
        logits[logits > 0.5] = 1
        logits[logits <= 0.5] = 0
        predictions = logits.squeeze().astype(int)
    return metric.compute(predictions=predictions, references=labels, average='macro') # {'f1': array([0.8, 0. , 0. , ...])}

def compute_metrics(eval_pred):
    logits, labels = eval_pred
    if len(labels.shape)>1:
        metric_dict = {}
        metric_dict['f1'] = []
        for task in range(labels.shape[-1]):
            res = compute_f1(logits[task], labels[:, task])
            metric_dict['f1'].append(res['f1'])
        metric_dict['f1'] = np.mean(metric_dict['f1'])
        return metric_dict
    else:
        return compute_f1(logits, labels)

class ClassificationHeadforMultiTask(nn.Module):
    def __init__(self, config, num_labels, predict_only):
        super().__init__()
        self.predict_only = predict_only
        self.num_labels = num_labels
        self.dense = nn.Linear(config.hidden_size, config.hidden_size)
        classifier_dropout = (config.classifier_dropout if config.classifier_dropout is not None else config.hidden_dropout_prob)
        self.dropout = nn.Dropout(classifier_dropout)
        if self.num_labels == 2:
            self.out_proj = nn.Linear(config.hidden_size, 1)
            self.loss_fct = BCEWithLogitsLoss()
        else:
            self.out_proj = nn.Linear(config.hidden_size, self.num_labels)
            self.loss_fct = CrossEntropyLoss()

    def forward(self, features, labels = None, **kwargs):
        x = features[:, 0, :]  # take <s> token (equiv. to [CLS])
        x = self.dropout(x)
        x = self.dense(x)
        x = torch.tanh(x)
        x = self.dropout(x)
        x = self.out_proj(x)
        x = torch.sigmoid(x)
        if not self.predict_only:
            if self.num_labels == 2:
                loss = self.loss_fct(x, labels)
            else:
                loss = self.loss_fct(x, labels.squeeze().long())
            
            return x, loss
        else:
            return x, None


class ClassificationHeadforExplicitMultiTask(nn.Module):
    def __init__(self, config, num_labels, dim_logit_features, predict_only):
        super().__init__()
        self.predict_only = predict_only
        self.num_labels = num_labels
        self.dense = nn.Linear(config.hidden_size, config.hidden_size)
        classifier_dropout = (config.classifier_dropout if config.classifier_dropout is not None else config.hidden_dropout_prob)
        self.dropout = nn.Dropout(classifier_dropout)
        self.explicit_classif_relu  = torch.nn.ReLU()
            
        if self.num_labels == 2:
            self.explicit_classif_proj = torch.nn.Linear(dim_logit_features, 1)
            self.out_proj = nn.Linear(config.hidden_size, 1)
            self.loss_fct = BCEWithLogitsLoss()
        else:
            self.explicit_classif_proj = torch.nn.Linear(dim_logit_features, self.num_labels)
            self.out_proj = nn.Linear(config.hidden_size, self.num_labels)
            self.combination_linear = nn.Linear(config.num_labels, self.num_labels)
            self.loss_fct = CrossEntropyLoss()

    def forward(self, features, concat_logits, labels, **kwargs):
        output_explicit = self.explicit_classif_relu(self.explicit_classif_proj(concat_logits))
        x = features[:, 0, :]  # take <s> token (equiv. to [CLS])
        x = self.dropout(x)
        x = self.dense(x)
        x = torch.tanh(x)
        x = self.dropout(x)
        x = self.out_proj(x)
        x = x + output_explicit
        if not self.predict_only:
            if self.num_labels == 2:
                loss = self.loss_fct(x, labels)
            else:
                loss = self.loss_fct(x, labels.squeeze().long())
            x = torch.sigmoid(x)
            return x, loss
        else:
            x = torch.sigmoid(x)
            return x, None


class ContextualizedClassificationHead(nn.Module):
    def __init__(self, config, num_labels):
        super().__init__()
        self.config = config
        self.num_labels = num_labels
        self.dense_prev = nn.Linear(config.hidden_size, 100)
        self.dense_central = nn.Linear(config.hidden_size, 100)
        self.dense_next = nn.Linear(config.hidden_size, 100)

        classifier_dropout = (config.classifier_dropout if config.classifier_dropout is not None else config.hidden_dropout_prob)
        self.dropout = nn.Dropout(classifier_dropout)
        if self.num_labels == 2:
            proj_output_size = 1
            self.loss_fct = BCEWithLogitsLoss()
        else:
            proj_output_size = self.num_labels
            self.loss_fct = CrossEntropyLoss()
        self.out_proj = nn.Linear(300, proj_output_size)

    def forward(self, features, labels, **kwargs):
        # features is a list of 3 Tensors of dim batch_size
        x = [torch.mean(batch_seq, dim=1) for batch_seq in features] # take <s> token (equiv. to [CLS])
        x_prev, x_central, x_next = self.dropout(x[0]), self.dropout(x[1]), self.dropout(x[2])
        x_prev = torch.tanh(self.dense_prev(x_prev))
        x_central = torch.tanh(self.dense_central(x_central))
        x_next = torch.tanh(self.dense_next(x_next))
        x_all = torch.cat([x_prev, x_central, x_next], 1)
        x_all = self.dropout(x_all)
        logits = self.out_proj(x_all)
        if self.num_labels == 2:
            loss = self.loss_fct(logits, labels.view(-1,1).float())
        else:
            loss = self.loss_fct(logits, labels.long())
        logits = torch.sigmoid(logits)
        
        return loss, logits


class SequenceMultiClassifierOutput(ModelOutput):
    """
    Base class for outputs of sentence classification models.
    """
    loss: Optional[torch.FloatTensor] = None
    logits: List[torch.FloatTensor] = None
    hidden_states: Optional[Tuple[torch.FloatTensor]] = None
    attentions: Optional[Tuple[torch.FloatTensor]] = None

class RobertaForContextualizedSequenceClassification(RobertaForSequenceClassification):
    def __init__(self, config, n_labels):
        super().__init__(config)
        self.classifier = ContextualizedClassificationHead(config, n_labels)

    def forward(
        self,
        input_ids: Optional[List[torch.LongTensor]] = None,
        attention_mask: Optional[List[torch.FloatTensor]] = None,
        token_type_ids: Optional[torch.LongTensor] = None,
        position_ids: Optional[torch.LongTensor] = None,
        head_mask: Optional[torch.FloatTensor] = None,
        inputs_embeds: Optional[torch.FloatTensor] = None,
        labels: Optional[torch.LongTensor] = None,
        output_attentions: Optional[bool] = None,
        output_hidden_states: Optional[bool] = None,
        return_dict: Optional[bool] = None,
    ) -> Union[Tuple[torch.Tensor], SequenceClassifierOutput]:

        return_dict = return_dict if return_dict is not None else self.config.use_return_dict
        seq_outputs = []

        for i in range(3):
            outputs = self.roberta(
                input_ids = torch.stack([ids[i] for ids in input_ids]),
                attention_mask=torch.stack([msk[i] for msk in attention_mask]),
                token_type_ids=token_type_ids,
                position_ids=position_ids,
                head_mask=head_mask,
                inputs_embeds=inputs_embeds,
                output_attentions=output_attentions,
                output_hidden_states=output_hidden_states,
                return_dict=return_dict,
            )
            seq_outputs.append(outputs[0])
        loss, logits = self.classifier(seq_outputs, labels)
       

        return SequenceClassifierOutput(
            loss=loss,
            logits=logits,
            #hidden_states=outputs.hidden_states,
            #attentions=outputs.attentions,
        )

class RobertaForSequenceMultiClassification(RobertaForSequenceClassification):

    def __init__(self, config, task_dict, device, args, features, predict_only=False):
        super().__init__(config)
        self.config = config
        self.roberta = RobertaModel(config, add_pooling_layer=False)
        self.task_dict = task_dict
        self.args = args
        self.features = features
        self.predict_only = predict_only
        nb_tasks=len(task_dict)
        
        if args.targetcol_explicit is not None:
            nb_tasks=len(task_dict) -1
            self.target_explicit = self.features[args.task][args.targetcol_explicit]
            # Separate the target and the other tasks when doing "explicit" MLT
            self.target_explicit_nb_labels = task_dict[self.target_explicit]
            self.classifiers = {task: ClassificationHeadforMultiTask(config, n_labels, self.predict_only).to(device) for task, n_labels in self.task_dict.items() if task != self.target_explicit} # Our special Multi-target classifier
            dim_all_logits = sum([1 if nblab==2 else nblab for nblab in self.task_dict.values() ]) - (1 if self.target_explicit_nb_labels==2 else self.target_explicit_nb_labels)
            self.explicit_classifier = ClassificationHeadforExplicitMultiTask(config, self.target_explicit_nb_labels, dim_all_logits, self.predict_only)
        else:
            self.classifiers = {task: ClassificationHeadforMultiTask(config, n_labels, self.predict_only).to(device) for task, n_labels in self.task_dict.items()} # Our special Multi-target classifier
        
        self.task_weighting = args.task_weighting
        if args.task_weighting:
            w = torch.ones(nb_tasks).to(device)
            w = f.normalize(w, p=1, dim=0)
            self.task_weights = nn.Parameter(w)
            self.saved_weights = None
        
        # Initialize weights and apply final processing
        self.post_init()
                

    def forward(
        self,
        input_ids: Optional[torch.LongTensor] = None,
        attention_mask: Optional[torch.FloatTensor] = None,
        token_type_ids: Optional[torch.LongTensor] = None,
        position_ids: Optional[torch.LongTensor] = None,
        head_mask: Optional[torch.FloatTensor] = None,
        inputs_embeds: Optional[torch.FloatTensor] = None,
        labels: Optional[List[torch.LongTensor]] = None, # list of torch Tensors, one per target task
        output_attentions: Optional[bool] = None,
        output_hidden_states: Optional[bool] = None,
        return_dict: Optional[bool] = None,
    ) -> Union[Tuple[torch.Tensor], SequenceMultiClassifierOutput]:
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        outputs = self.roberta(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
            return_dict=return_dict,
        )
        sequence_output = outputs[0]
        #labels_dict = {task: torch.cat([t.view(1, ) for t in [labels[idx][i] for idx in range(len(labels))]]) for i, task in enumerate(features[args.task])}
        if not self.predict_only:
            labels_dict = {task: torch.tensor([labels[idx][i] for idx in range(len(labels))], dtype = torch.float, device=sequence_output.device).unsqueeze(1) for i, task in enumerate(self.task_dict.keys())}
            classifier_outputs = {task: classifier(sequence_output, labels_dict[task]) for task, classifier in self.classifiers.items()}
            
            if self.task_weighting:
                all_loss = torch.stack([x[1] for x in classifier_outputs.values()])
                all_loss = all_loss * f.normalize(self.task_weights, p=1, dim=0)
                loss = sum(all_loss)
                task_weights = f.normalize(self.task_weights, p=1, dim=0)
                self.saved_weights = task_weights.detach().cpu().numpy().tolist()
            else:
                loss = sum(map(operator.itemgetter(1),classifier_outputs.values()))
                task_weights = None

            logits = [classifier_outputs[task][0] for task in self.classifiers.keys()]
            
            if self.args.targetcol_explicit is not None:
                all_logit = torch.cat(logits, 1)
                logits, explicit_classif_loss = self.explicit_classifier(sequence_output, all_logit, labels_dict[self.target_explicit])
                loss = explicit_classif_loss + loss
        else: # Only for inference, without labels
            classifier_outputs = {task: classifier(sequence_output) for task, classifier in self.classifiers.items()} 
            logits = [classifier_outputs[task][0] for task in self.classifiers.keys()]
            loss = None
            if self.args.targetcol_explicit is not None:
                all_logit = torch.cat(logits, 1)
                logits, _ = self.explicit_classifier(sequence_output, all_logit, labels_dict[self.target_explicit])
                    
        return SequenceMultiClassifierOutput(
            loss=loss,
            logits=logits,
            #hidden_states=outputs.hidden_states,
            #attentions=outputs.attentions,
        )