import torch
import pandas as pd
from collections import Counter
import numpy as np
from sklearn.model_selection import train_test_split


class ESGDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels.to_list()

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['label'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)

class ESGContextualizedDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels.to_list()
        self.input_keys = ['ids', 'type_ids', 'tokens', 'attention_mask']

    def __getitem__(self, idx):
        subitem = self.encodings[idx]
        item = {key:[torch.tensor(val) for val in values] for key, values in subitem.items()}
        item['label'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)

class ESGDatasetMultitask(torch.utils.data.Dataset):
    def __init__(self, encodings, labels, args, features):
        self.encodings = encodings
        self.labels = labels
        self.args = args
        self.features = features

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        #item['label'] = {task: torch.tensor(self.labels[task][idx]) for task in self.labels}
        item['label'] = [torch.tensor(self.labels[i][idx]) for i in range(len(self.args.target_cols))]
        return item

    def __len__(self):
        return len(self.labels[0])

def prepare_data(dataframe, tokenizer, args, features):
    # create datasets dict
    texts = dataframe.text.astype("string").tolist()
    if args.surrounding and args.task == 'part2_new':
        texts = [sent.split('[SEP]') for sent in texts]
        encodings = []
        for sublist in texts:
            sublist_encodings = tokenizer(sublist, truncation=True, padding="max_length", max_length=128)
            encodings.append(sublist_encodings)
    else:
        encodings = tokenizer(texts, truncation=True, padding=True)
    if 'label' not in dataframe.columns: # if 'label', it means we are not in a multitask fashion
        targets = features[args.task]
        labels = [list(dataframe[col]) for col in dataframe.columns if col in targets]
        dataset = ESGDatasetMultitask(encodings, labels, args, features)
    else:
        labels = dataframe['label']
        if args.surrounding and args.task == "part2_new":
            dataset = ESGContextualizedDataset(encodings, labels)
        else:
            dataset = ESGDataset(encodings, labels)
    return dataset

def encode_label_merge(l):
    # merge these two pairs of labels
    if l in ['Human capital', 'Social capital']:
        return 'Human & Social capital'
    if l in ['Leadership and Governance', 'Business Model and Innovation']:
        return 'Leadership, Governance, Business & Innovation'
    if l in ['General ESG', "Not ESG"]:
        return np.nan
    else:
        return l

def encode_label_delete(l):
    # merge these two pairs of labels
    if l in ['General ESG', "Not ESG"]:
        return np.nan
    else:
        return l

def load_data(df, target_col, args):
    if args.task == 'part2_new':
        if args.surrounding:
            df['text'] = df['Previous sentence'] + '[SEP]' + df['Sentence to annotate'] + '[SEP]' + df['Next sentence']
            df = df[[target_col, 'text']].rename(columns={target_col: 'label'})
        else:
            df = df[[target_col, 'Sentence to annotate']].rename(columns={target_col: 'label', 'Sentence to annotate': 'text'})
        # there are some missing values in the sentences! Probably ones without a previous or next sentence? 
        df = df.dropna(subset=['text']) #pd.isnull(t)
        if target_col=='ESG type':
            if args.merge_part2:
                df['label'] = [encode_label_merge(l) for l in list(df['label'])]
            else:
                df['label'] = [encode_label_delete(l) for l in list(df['label'])]
            df.dropna(subset=['label'], inplace=True)
            #df['label'].fillna('Not ESG', inplace=True)
    else:
        df = df[[target_col, 'Sentences']].rename(columns={target_col: 'label', 'Sentences': 'text'})
    # fill missing values and map labels to ints
    if 'old' in args.task:
        df['label'] = df['label'].fillna(0).replace({-1.0:2}).astype(int).astype('category') 
        map_dict = {}
    else:
        map_dict = {lab:i for i, lab in enumerate(set(df['label']))}
        df['label'] = df['label'].replace(map_dict).astype('category') 
    print(target_col, 'labels distribution:', Counter(df['label']))
    train, test = train_test_split(df, test_size=0.2, random_state=42)
    train, val = train_test_split(train, test_size=0.2, random_state=42)
    return [train, val, test], map_dict

def load_data_multitask(df, target_cols_list, args):
    if args.task == 'part2_new':
        df = df[target_cols_list + ['Sentence to annotate']].rename(columns={'Sentence to annotate': 'text'})
        df = df.dropna(subset=['text']) #pd.isnull(t)
        df['ESG type'].fillna('Not ESG', inplace=True)
    else:
        df = df[target_cols_list + ['Sentences']].rename(columns={'Sentences': 'text'})
    # fill missing values and map labels to ints
    map_dicts = {}
    for col in target_cols_list:
        map_dict = {lab:i for i, lab in enumerate(set(df[col]))}
        df[col] = df[col].replace(map_dict).astype('category') 
        map_dicts[col] = map_dict
        print(col, 'labels distribution:', Counter(df[col]))
    train, test = train_test_split(df, test_size=0.2, random_state=42)
    train, val = train_test_split(train, test_size=0.2, random_state=42)
    return [train, val, test], map_dicts

def load_json_data(args):
    df = pd.read_json('data/FinSim-ESG training set/data/sentences/Sustainability_sentences_train.json')
    df = df.rename(columns={'sentence': 'text'}).replace({'sustainable':1, 'unsustainable':0}).astype('category') 
    print('labels distribution:', Counter(df['label']))
    train, test = train_test_split(df, test_size=0.2, random_state=42)
    train, val = train_test_split(train, test_size=0.2, random_state=42)
    return train, val, test
