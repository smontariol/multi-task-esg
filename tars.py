from transformers import AutoTokenizer, AutoModelForSequenceClassification, AutoConfig
from torch.utils.data import RandomSampler, SequentialSampler, DataLoader
from sklearn.model_selection import train_test_split
from sklearn.metrics import recall_score, classification_report
import torch
import pandas as pd
import numpy as np

import random
import argparse
import os
from math import floor
import json

from src.dataset_utils_tars import *
from src.dataset_utils import *
from src.BertClassificationTraining import *


def run():
    """This script only works when you use 10% of the sorted corpus as evaluation data!
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("--output_dir",
                        required=True,
                        type=str)

    parser.add_argument("--tokenizer_file",
                        type=str)
    parser.add_argument("--config_file",
                        type=str)
    parser.add_argument("--model_file",
                        type=str)
    parser.add_argument("--task",
                        type=str)

    parser.add_argument("--eval_split",
                        default=0.2,
                        type=float)
    parser.add_argument("--test_split",
                        default=0.1,
                        type=float)
    parser.add_argument("--max_len",
                        default=512,
                        type=int)
    parser.add_argument("--batch_size",
                        default=8,
                        type=int)
    parser.add_argument("--num_epochs",
                        default=3,
                        type=int)
    parser.add_argument("--learning_rate",
                        default=2e-5,
                        type=float)
    parser.add_argument("--weight_decay",
                        default=0.01,
                        type=float)
    parser.add_argument("--warmup_proportion",
                        default=0.1,
                        type=float)
    parser.add_argument("--adam_epsilon",
                        default=1e-8,
                        type=float)
    parser.add_argument("--seed",
                        default=42,
                        type=int)

    args = parser.parse_args()

    if not os.path.exists(args.output_dir):
        os.mkdir(args.output_dir)

    print("Setting the random seed...")
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    output_dir = args.output_dir
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    
    data_paths = {'esg_old': 'data/esg_data.csv', \
                'sentiment_old':'data/sentences_financial_sentiment.csv',\
                'part1_new':'data/new_annotations_part1_dedup.csv',\
                'part2_new':'data/new_annotations_part2_dedup.csv'}
    task_targets = {'esg': ['non-esg', 'esg'], \
                'sentiment':['neutral', 'positive', 'negative'],
                'finsim':['unsustainable', 'sustainable']}

    features = {'part1_new': ['Relevance[business related text / general text]',\
       'Financial sentiment[positive/neutral/negative]',\
       'Objectivity[objective/subjective]', 'Forward looking[FLS/non-FLS]',\
       'ESG[ESG related/not ESG related]'], \
        'part2_new': ['ESG/not-Esg', 'ESG type']}

    labels_dict = {'Relevance[business related text / general text]': ["business", "general"],
            'Financial sentiment[positive/neutral/negative]': ["negative", "neutral", "positive"],
            'Objectivity[objective/subjective]': ["objective", "subjective"],
            'Forward looking[FLS/non-FLS]': ["FLS", "non-FLS"],
            'ESG[ESG related/not ESG related]': ["ESG", "non-ESG"]}

    print("Reading data...")
    df = pd.read_csv(data_paths[args.task])
    #args.target_cols = [str(i) for i in range(len(features[args.task]))]
    target_cols = features[args.task]
    splits, map_dicts = load_data_multitask_tars(df, target_cols, args)
    
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    print("Loading pre-trained model...")

    tokenizer = AutoTokenizer.from_pretrained("roberta-base")
    config = AutoConfig.from_pretrained("./models/roberta-ftse/", num_labels=2)
    model = AutoModelForSequenceClassification.from_pretrained("./models/roberta-ftse/", config=config)

    bert_trainer = BertClassificationTraining(model, device, tokenizer, batch_size=args.batch_size,
                                               lr=args.learning_rate, train_epochs=args.num_epochs,
                                               weight_decay=args.weight_decay,
                                               warmup_proportion=args.warmup_proportion,
                                              adam_epsilon=args.adam_epsilon)

    print("Preparing data...")
    train_dataset = prepare_data_tars(splits[0], tokenizer, labels_dict, args.max_len, args.batch_size)
    sampler = RandomSampler(train_dataset)
    train_dataloader = DataLoader(train_dataset, sampler=sampler, batch_size=args.batch_size)
    eval_dataset = prepare_data_tars(splits[1], tokenizer, labels_dict, args.max_len, args.batch_size)
    sampler = RandomSampler(eval_dataset)
    eval_dataloader = DataLoader(eval_dataset, sampler=sampler, batch_size=args.batch_size)

    print("Training...")
    bert_trainer.train(train_dataloader, eval_dataloader, output_dir, save_best=True)

    print("Evaluating...")
    config = AutoConfig.from_pretrained(args.output_dir)
    model = AutoModelForSequenceClassification.from_pretrained(args.output_dir, config=config)

    bert_trainer = BertClassificationTraining(model, device, tokenizer, batch_size=args.batch_size,
                                               lr=args.learning_rate, train_epochs=args.num_epochs,
                                               weight_decay=args.weight_decay,
                                               warmup_proportion=args.warmup_proportion,
                                               adam_epsilon=args.adam_epsilon)

    test_data = splits[2]
    for col in target_cols:
        labels_list = sorted(list(set(labels_dict[col])))
        predicted = []
        gold = []
        for index, row in test_data.iterrows():
            print(col)
            gold.append(labels_list.index(row[col]))
            predictions = []
            for l in labels_list:
                data = []
                labels = []
                data.append(str(l) + " [SEP] " + row['text'])
                if l == row[col]:
                    labels.append(1)
                else:
                    labels.append(0)
                instance = prepare_dataset(data, labels, tokenizer, args.max_len, 1)
                sampler = SequentialSampler(instance)
                instance_dataloader = DataLoader(instance, sampler=sampler, batch_size=1)
                _, probabilities = bert_trainer.predict(instance_dataloader, return_probabilities=True)
                predictions.append(probabilities[0][1])
            predicted.append(np.argmax(predictions))
        results = classification_report(gold, predicted, output_dict=True)
        log_path = os.path.join(output_dir, col.split("/")[0] + "_log.json")
        with open(log_path, 'w') as fp:
                json.dump(results, fp, indent=4)
        
    print("Done.")


if __name__ == "__main__":
    run()
