import os
import glob

for filepath in glob.glob('data/ftse350_corpus_raw/*/*.pdf'):
    print(filepath)
    newfilepath = filepath.replace('.pdf', '.txt')
    if not os.path.exists(newfilepath):
        os.system(f"python pdfminer.six/tools/pdf2txt.py {filepath} > {newfilepath}")

