import re
import argparse
import os
import string
from tqdm import tqdm
import pandas as pd
import glob
import os
import re
import argparse
from rich.console import Console


alphabets= "([A-Za-z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|net|org|io|gov)"

def split_into_sentences(text):
    """
    Source: https://stackoverflow.com/questions/4576077/how-can-i-split-a-text-into-sentences
    """
    text = " " + text + "  "
    text = text.replace("\n"," ")
    text = re.sub(prefixes,"\\1<prd>",text)
    text = remove_url(text, '')
    if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
    text = re.sub("\s" + alphabets + "[.] "," \\1<prd> ",text)
    text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>",text)
    text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
    text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
    text = re.sub(" " + alphabets + "[.]"," \\1<prd>",text)
    if "”" in text: text = text.replace(".”","”.")
    if "\"" in text: text = text.replace(".\"","\".")
    if "!" in text: text = text.replace("!\"","\"!")
    if "?" in text: text = text.replace("?\"","\"?")
    text = text.replace(".",".<stop>")
    text = text.replace("•",".<stop>")
    text = text.replace("?","?<stop>")
    text = text.replace("!","!<stop>")
    text = text.replace("<prd>",".")
    sentences = text.split("<stop>")
    sentences = sentences[:-1]
    sentences = [s.strip() for s in sentences]
    return sentences

def remove_url(text, replace_token):
    regex = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    return re.sub(regex, replace_token, text)

def get_numbers_words_ratio(sent):
    """
    Calculates the ratio between the number of words and numbers in a sentence
    :param str sent: input sentence
    :return float: the ratio between words and numbers in an input sentence 
    """
    sent = sent.replace(",", " ")
    sent = sent.split()
    numeric = 0
    alphanumeric = 0
    for token in sent:
        if token.isnumeric():
            numeric += 1
        alphanumeric += 1
    ratio = numeric / alphanumeric
    return ratio

def get_letters_ratio(sent):
    return sum([token.isalpha() for token in sent])/len(sent)

def get_proportion_capital_words(sent):
    words = sent.split()
    return len([word for word in words if word.isupper()])/len(words)
    

def check_sentence(sent, letter_char_ratio, numbers_words_ratio, min_length):
    """
    :param float letter_char_ratio: minimum ratio between characters and letter a sentence can have
    :param float number_words_ratio: maximum ratio between words and numbers a sentence can have
    :param int min_length: minimum length of the sentence in words
    """
    sent = sent.strip()
    if sent.isupper() \
        or (get_letters_ratio(sent) < letter_char_ratio) \
        or len(sent) > 400\
        or sent[0].islower()\
        or not sent[0].isalpha() \
        or sent[-1] not in string.punctuation\
        or (get_numbers_words_ratio(sent) > numbers_words_ratio)\
        or (len(sent.split()) < min_length) \
        or len(re.findall('(\s+\w\s+\w)',sent)) > 5\
        or get_proportion_capital_words(sent) > 0.25\
        or re.search("cid:", sent):
        #print('OUT:', sent)
        return None
    else:
        sent = re.sub(r'\s+', ' ',   sent)  # eliminate duplicated whitespaces
        return sent


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--folder', '-f', help="path to folder containing your text files", type=str, default='data/ftse350/')
    parser.add_argument('--output', '-o', help="output folder", type=str, default="data/ftse350_corpus_preprocessed/")

    args = parser.parse_args()
    console = Console(record=True)

    inputFolder = args.folder
    outputFolder = args.output

    companies_paths = sorted(os.listdir(inputFolder))
    for company_ticker in tqdm(companies_paths):
        if not os.path.exists(outputFolder + company_ticker):
            os.mkdir(outputFolder + company_ticker)
        # Go through all the .txt files within the input folder.
        for txtfile in glob.glob(inputFolder + company_ticker + '/LSE_*.txt'):
            nbsent=0
            with open(txtfile, encoding="utf8") as input:
                text = input.read()
            filename = os.path.basename(txtfile).replace('LSE_','')
            with open(outputFolder + company_ticker + '/' + filename, 'w+', encoding="utf8") as output:
                sentences = split_into_sentences(text)
                # Go through all the sentences within a text file.
                for sent in sentences:
                    processed_sentence = check_sentence(sent, letter_char_ratio = 0.7, numbers_words_ratio=0.15, min_length=12)
                    if processed_sentence:
                        nbsent += 1
                        output.write(processed_sentence+'\n')
            console.print(filename[:-4], nbsent)