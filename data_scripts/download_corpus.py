import csv
import os

exchange = 'LSE'
years = [2021]
tickers = os.listdir("data/ftse350_corpus_preprocessed")
savepath = 'data/ftse350_corpus_raw/'

for t in tickers:
    filepath = savepath + t
    if not os.path.exists(filepath):
        os.mkdir(filepath)
    firstLetterU=t[0].upper()
    firstLetterL=t[0].lower()
    if (t == "MRW"):
    # W presumably for "Wm Morrisons"
        firstLetterU='W'
        firstLetterL='w'
    if (t == "GB0767628"):
    # J presumably for "J Sainsbury's"
        firstLetterU='J'
        firstLetterL='j'
    for y in years:
        fileName = "%s_%s_%s.pdf" % (exchange, t, y)
        print(fileName)
        # download exchange_t_y.pdf
        os.system("wget https://www.annualreports.com/HostedData/AnnualReports/PDF/" + fileName + ' -P ' + filepath)
        os.system("wget https://www.annualreports.com/HostedData/AnnualReportArchive/" + firstLetterL + "/" + fileName + ' -P ' + filepath)
