from transformers import AutoTokenizer, AutoModelForSequenceClassification, TrainingArguments, Trainer
import pandas as pd
import numpy as np
from torch import cuda
import argparse
import os
import json
import logging
from sklearn.metrics import classification_report
from rich.console import Console
import ray
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import PopulationBasedTraining


from src.models_utils import *
from src.dataset_utils import *
from src.transformers_utils import *


# define a rich console logger
console = Console(record=True)

os.environ["WANDB_DISABLED"] = "true"
logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)

device = 'cuda' if cuda.is_available() else 'cpu'

"""
A100-PCIE-40GB with CUDA capability sm_80 is not compatible with the current PyTorch installation.
The current PyTorch install supports CUDA capabilities sm_37 sm_50 sm_60 sm_70.
If you want to use the A100-PCIE-40GB GPU with PyTorch, please check the instructions at https://pytorch.org/get-started/locally/
pip install torch==1.8.0+cu111 torchvision==0.9.0+cu111 torchaudio==0.8.0 -f https://download.pytorch.org/whl/torch_stable.html
"""


def model_init():
    return AutoModelForSequenceClassification.from_pretrained(args.model_path, num_labels=len(task_targets[target_col]), return_dict=True)


parser = argparse.ArgumentParser()
parser.add_argument("-e", "--eval", action="store_true", default=False,
                help="Boolean: do you want to do evaluation only?")
parser.add_argument("-mt", "--multitask", action="store_true", default=False,
                help="Boolean: do you want to train all tasks jointly? Only works for task=part1_new and transformers model.")
parser.add_argument("-g", "--gridsearch", action="store_true", default=False,
                help="Boolean: do you want to do gridsearch or use default parameters?")
parser.add_argument("-m", "--model", type=str, default="logistic",
                help="Which model to use: logistic, svm")
parser.add_argument("--transformers_base_model", type=str, default="roberta-base",
                help="The base model to use for the tokenizer.")
parser.add_argument("--model_path", type=str, default="models/roberta-ftse/",
                help="Path to the fine-tuned transformers model.")
parser.add_argument("-enc", "--encoder", type=str, default="transformer",
                help="Which model to use: tfidf, transformer")
parser.add_argument("-sur", "--surrounding", action="store_true", default=False,
                help="Boolean: use the surrounding sentences for classification or not. Works only for task=part2_new and transformers model.")
parser.add_argument("-t", "--task", type=str, default="esg",
                help="Which task to solve: sentiment_old or esg_old classification or part1_new (in that case, use all variables in a loop).\
                    also use 'finsim' for FinSim-ESG shared task.")
parser.add_argument("-tc", "--target_cols", nargs="+",
                    help="Which column of the dataset to use as target, if not all. Use col numbers: 0 4, 0 1 2, ...")
parser.add_argument("-tw", "--task_weighting", action="store_true", default=False,
                help="Boolean: If True, learn weights for specific tasks, only applicable when multitask is true")
parser.add_argument("-seq", "--sequential_training", action="store_true", default=False,
                help="Boolean: fine-tune on each task seprately after training on several other tasks (use with -mt flag).")
parser.add_argument("-ep", "--epochs", type=int, default = 1,
                help="Number of training epochs")
parser.add_argument("--seed", type=int, default = 1,
                help="Seed to use for training.")
parser.add_argument("-s", "--save_results", action="store_true", default=False,
                help="Boolean: save the final performance as dictionary (when not testing code).")
parser.add_argument("-sm", "--save_model", action="store_true", default=False,
                help="Boolean: save the results.")
parser.add_argument("-tce", "--targetcol_explicit", type=int, default=None,
                help="Target column for 'explicit' multi-task training (adding other tasks logits as features). Should be the nb of the column.")
parser.add_argument("--merge_part2", action="store_true", default=False,
                help="Boolean: Merge human & social capital and remove General ESG in Part2 - ESG type.")


if __name__ == "__main__":
    args = parser.parse_args()
    set_seed(args.seed)

    data_paths = {'esg_old': 'data/esg_data.csv', \
                'sentiment_old':'data/sentences_financial_sentiment.csv',\
                'part1_new':'data/new_annotations_part1_dedup.csv',\
                'part2_new':'data/new_annotations_part2_dedup.csv',\
                'esg_new':'data/esg_data_new.csv'}
    task_targets = {'esg': ['non-esg', 'esg'], \
                'sentiment':['neutral', 'positive', 'negative'],
                'finsim':['unsustainable', 'sustainable']}

    features = {'part1_new': ['Relevance[business related text / general text]',\
       'Financial sentiment[positive/neutral/negative]',\
       'Objectivity[objective/subjective]', 'Forward looking[FLS/non-FLS]',\
       'ESG[ESG related/not ESG related]'], \
        'part2_new': ['ESG/not-Esg', 'ESG type'],\
        'esg_new': ['ESG[ESG related/not ESG related]']}

    splits = {}
    if args.task in data_paths:
        df = pd.read_csv(data_paths[args.task])
        if args.task == 'esg_old':
            target_cols = ['ESG[ESG related/not ESG related]']
        elif args.task == 'sentiment_old':
            target_cols = ['Financial sentiment[positive/neutral/negative]']
        else:
            if args.target_cols:
                # In case we want to do the Explicit MLT, need to make sure the target task is in our data.
                if args.targetcol_explicit is not None and args.targetcol_explicit not in args.target_cols:
                    args.target_cols.append(args.targetcol_explicit)
                target_cols = [features[args.task][int(tc)] for tc in args.target_cols]
            else:
                args.target_cols = [str(i) for i in range(len(features[args.task]))]
                target_cols = features[args.task]
        
        if args.multitask:
            splits['multitask'], map_dicts = load_data_multitask(df, target_cols, args)
            for col in map_dicts:
                task_targets[col] = map_dicts[col].keys()
        else:
            for col in target_cols:
                splits[col], map_dict = load_data(df, col, args)
                if 'new' in args.task:
                    task_targets[col] = map_dict.keys()
    elif args.task == 'finsim':
        splits['sustainability'] = load_json_data(args)
    else:
        print("task name not recognized")

    results_all_dict = {}
    for target_col, split in splits.items():
        target_col_name = target_col.split('[')[0].replace(' ', '_')
        print('########################', target_col_name, '################################')
        train, val, test = split[0], split[1], split[2]

        if args.encoder == 'tfidf':
            tfidf_vectorizer, train_tfidf, val_tfidf = tfidf_vectorize(train, val)

            if args.model == 'logistic':
                val_pred = logistic_regression(train_tfidf, val_tfidf, train, tfidf_vectorizer, args)
            elif args.model == 'svm':
                val_pred = svm(train_tfidf, val_tfidf, train, args)


        elif args.encoder == 'transformer':
            batch_size = 12 if not args.surrounding else 12
            tokenizer = AutoTokenizer.from_pretrained(args.transformers_base_model)
            df_train = prepare_data(train, tokenizer, args, features)
            df_val = prepare_data(val, tokenizer, args, features)
            df_test = prepare_data(test, tokenizer, args, features)
            
            
            metrics_fct = compute_metrics
            model_ouput_dir = args.model_path + "Checkpoints/" + target_col_name + str(args.seed) + '-'.join(args.target_cols)
            if args.save_model:
                model_ouput_dir = args.model_path + "/final_" + args.task + '/seed_' + str(args.seed)
                if args.merge_part2:
                    model_ouput_dir = args.model_path + "/final_" + args.task + '_merged/seed_' + str(args.seed)
            
            if args.targetcol_explicit is not None:
                if args.save_model:
                    model_ouput_dir = args.model_path + "/final_EXPL/target_" + str(args.targetcol_explicit) + '/' + 'seed_' + str(args.seed)
                metrics_fct = None # we get one logits and 5 labels and hard to get the information on which is the target task !
            
            if args.multitask:
                task_dict = {task:len(labels) for task, labels in map_dicts.items()}
                model = RobertaForSequenceMultiClassification.from_pretrained(args.model_path, task_dict, device, args, features)
            else:
                if args.surrounding:
                    model = RobertaForContextualizedSequenceClassification.from_pretrained(args.model_path, n_labels = len(task_targets[target_col]))
                else:
                    model = AutoModelForSequenceClassification.from_pretrained(args.model_path, num_labels=len(task_targets[target_col]))
            model = model.to(device)
            
            training_args = TrainingArguments(
                output_dir= model_ouput_dir,
                learning_rate=6e-5,
                per_device_train_batch_size=batch_size,
                per_device_eval_batch_size=batch_size,
                num_train_epochs=args.epochs,
                evaluation_strategy = 'epoch',
                save_strategy = 'epoch',
                save_total_limit = 1, # Only last model is saved. Older ones are deleted.
                weight_decay=0.02,               # strength of weight decay
                load_best_model_at_end = True,
                metric_for_best_model = 'eval_f1' if not args.multitask else "loss",
                seed = args.seed,
            )
            console.log(training_args)

            trainer = Trainer(
                model=model if not args.gridsearch else None,
                model_init=model_init if args.gridsearch else None,
                args=training_args,
                train_dataset=df_train,
                eval_dataset=df_val,
                tokenizer=tokenizer,
                compute_metrics=metrics_fct,
            )
            if args.gridsearch:
                # now this is where you can define your hyperparam space for Tune
                scheduler = PopulationBasedTraining(
                    time_attr="training_iteration",
                    metric="eval_f1",
                    mode="max",
                    perturbation_interval=1,
                    hyperparam_mutations={
                        "weight_decay": tune.choice([0.1, 0.2, 0.3]),
                        "learning_rate": tune.choice([1e-4, 5e-4, 1e-5, 3e-5, 5e-5]),
                        "per_device_train_batch_size": tune.choice([8, 16, 32]),
                    },
                )

                reporter = CLIReporter(
                    parameter_columns={
                        "weight_decay": "w_decay",
                        "learning_rate": "lr",
                        "per_device_train_batch_size": "train_bs/gpu",
                        "num_train_epochs": "num_epochs",
                    },
                    metric_columns=["eval_f1", "eval_loss", "epoch", "training_iteration"],
                )
                tune_config = {
                    "num_train_epochs": tune.choice([3, 5, 8]),
                }

                trainer.hyperparameter_search(
                    hp_space=lambda _: tune_config,
                    backend="ray",
                    n_trials=25,
                    resources_per_trial={"cpu": 1, "gpu": 1},
                    scheduler=scheduler,
                    keep_checkpoints_num=1,
                    checkpoint_score_attr="training_iteration",
                    progress_reporter=reporter,
                    local_dir="~/ray_results/",
                    name="tune_transformer_formica_monotask",
                    log_to_file=True,
                )

            trainer.train()
            results = trainer.predict(df_test)
            if args.save_model:
                trainer.save_model(model_ouput_dir)

        
        # save results
        res_path = 'models/results_metrics/' + args.task
        if args.merge_part2:
            res_path = res_path + '_merged/'
        if not os.path.exists(res_path):
            os.mkdir(res_path)
        if args.task == 'part1_new':
            if args.multitask and not args.task_weighting:
                res_path = res_path + '/multi/' 
            elif args.task_weighting:
                res_path = res_path + '/multi_weighted/'
            else:
                res_path = res_path + '/mono/'
        elif args.task == 'part2_new':
            res_path = res_path + '/context/' if args.surrounding else res_path + '/central/'
        if not os.path.exists(res_path):
            os.mkdir(res_path)
        if not os.path.exists(res_path + '/' + '-'.join(args.target_cols) +'/'):
            os.mkdir(res_path + '/' + '-'.join(args.target_cols) +'/')
        
        if args.multitask:
            if args.targetcol_explicit is not None:
                results_all_dict = {}
                task = features[args.task][args.targetcol_explicit]
                target_col_name = task.split('[')[0].replace(' ', '_')
                print("################ Explicit MTL:", target_col_name, "#######################")
                preds=[]
                for row in results.predictions:
                    if len(row)==1:
                        preds.append(int(row>0.5))
                    else:
                        preds.append(int(np.argmax(row)))
                val_pred = pd.Series(preds)
                console.print(classification_report(val_pred, test[task], target_names=task_targets[task]))
                report_dict = classification_report(val_pred, test[task], target_names=task_targets[task], output_dict=True)
                results_all_dict[target_col_name] = report_dict
                if args.save_results:
                    if not os.path.exists(res_path + '/' + '-'.join(args.target_cols) + '+expl' + str(args.targetcol_explicit) +'/'):
                        os.mkdir(res_path + '/' + '-'.join(args.target_cols) +'+expl' + str(args.targetcol_explicit) +'/')
                    with open(res_path + '/' + '-'.join(args.target_cols) +'+expl' + str(args.targetcol_explicit) + '/' + str(args.seed) + '.json', 'w') as fp:
                        json.dump(results_all_dict, fp, indent=4)
            
            else:
                # output the results for all tasks
                results_all_dict = {}
                if args.task_weighting:
                    results_all_dict['task_weights'] = model.saved_weights
                for i, task in enumerate(map_dicts.keys()):
                    target_col_name = task.split('[')[0].replace(' ', '_')
                    print("################", target_col_name, "#######################")
                    preds = []
                    for row in results.predictions[i]: # for each example
                        if len(row)==1:
                            preds.append(int(row>0.5))
                        else:
                            preds.append(int(np.argmax(row)))
                    task_pred = pd.Series(preds)
                    console.print(classification_report(task_pred, test[task], target_names=task_targets[task]))
                    report_dict = classification_report(task_pred, test[task], target_names=task_targets[task], output_dict=True)
                    results_all_dict[target_col_name] = report_dict
                if args.save_results:
                    with open(res_path + '/' + '-'.join(args.target_cols) +'/' + str(args.seed) + '.json', 'w') as fp:
                        json.dump(results_all_dict, fp, indent=4)


        if args.sequential_training:
            # After training on a set of tasks (multitask), fine-tune on the "main" one
            trained_encoder = model.roberta
            splits = {}
            #args.target_cols = [str(i) for i in range(len(features[args.task]))]
            target_cols = features[args.task]
            for col in target_cols:
                splits[col], map_dict = load_data(df, col, args)
                task_targets[col] = map_dict.keys()
            results_all_dict = {}
            for target_col, split in splits.items():
                target_col_name = target_col.split('[')[0].replace(' ', '_')
                print('######################## Final task:', target_col_name, '################################')
                train, val, test = split[0], split[1], split[2]
                seq_df_train = prepare_data(train, tokenizer, args, features)
                seq_df_val = prepare_data(val, tokenizer, args, features)
                seq_df_test = prepare_data(test, tokenizer, args, features)
                metrics_fct = compute_metrics
                model_seq = AutoModelForSequenceClassification.from_pretrained(args.model_path, num_labels=len(task_targets[target_col]))
                model_seq.roberta = trained_encoder
                trainer = Trainer(
                    model=model_seq,
                    args=training_args,
                    train_dataset=seq_df_train,
                    eval_dataset=seq_df_val,
                    tokenizer=tokenizer,
                    compute_metrics=metrics_fct,
                )
                trainer.train()
                results = trainer.predict(seq_df_test)
                preds = []
                for row in results.predictions: # for each example
                    # we use our own model which uses alternatively cross-entropy loss and BCE loss, so need to do the following distinction:
                    if len(row)==1:
                        print('Should not happen, check!!!!!')
                        preds.append(int(row>0.5))
                    else:
                        preds.append(int(np.argmax(row)))
                task_pred = pd.Series(preds)
                console.print(classification_report(task_pred, test['label'], target_names=task_targets[target_col]))
                report_dict = classification_report(task_pred, test['label'], target_names=task_targets[target_col], output_dict=True)
                results_all_dict[target_col_name] = report_dict
            if args.save_results:
                if not os.path.exists(res_path + '/' + '-'.join(args.target_cols) + '+seq' +'/'):
                    os.mkdir(res_path + '/' + '-'.join(args.target_cols) +'+seq' + '/')
                with open(res_path + '/' + '-'.join(args.target_cols) + '+seq' +'/' + str(args.seed)+ '.json', 'w') as fp:
                    json.dump(results_all_dict, fp, indent=4)

        if not args.multitask and not args.sequential_training:
            # store evaluation metrics for the target tasks (monotask training)
            preds=[]
            for row in results.predictions:
                if len(row) == 1:
                    preds.append(int(row > 0.5))
                else:
                    preds.append(int(np.argmax(row)))
            val_pred = pd.Series(preds)
            console.print(classification_report(val_pred, test['label'], target_names=task_targets[target_col]))
            report_dict = classification_report(val_pred, test['label'], target_names=task_targets[target_col], output_dict=True)
            results_all_dict[target_col_name] = report_dict
            print(task_targets[target_col])
        
    # save results for all tasks in the same file, when training in a monotask fashion
    if not args.multitask and args.save_results:
        with open(res_path + '/' + '-'.join(args.target_cols) +'/' + str(args.seed)+ '.json', 'w') as fp:
            json.dump(results_all_dict, fp, indent=4)
        console.save_text(res_path + 'log.txt')


