from transformers import AutoTokenizer

import pandas as pd
import numpy as np
from torch import cuda
import argparse
import os
import logging
from tqdm import tqdm
import glob

from src.models_utils import *
from src.dataset_utils import *
from src.transformers_utils import *



os.environ["WANDB_DISABLED"] = "true"
logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)

device = 'cuda' if cuda.is_available() else 'cpu'


parser = argparse.ArgumentParser()
parser.add_argument('-m', "--model_path", type=str, default="models/roberta-ftse/multitask_final/checkpoint-1060",
                help="Path to the fine-tuned transformers model.")
parser.add_argument('--input', '-i', help="input folder", type=str, default="data/ftse350_corpus_preprocessed/")
parser.add_argument("-mt", "--multitask", action="store_true", default=True,
            help="Boolean: do you want to train all tasks jointly? Only works for task=part1_new and transformers model.")
parser.add_argument("-sur", "--surrounding", action="store_true", default=False,
                help="Boolean: use the surrounding sentences for classification or not. Works only for task=part2_new and transformers model.")
parser.add_argument("-t", "--task", type=str, default="part1_new",
                help="Which task to solve: sentiment_old or esg_old classification or part1_new (in that case, use all variables in a loop).\
                    also use 'finsim' for FinSim-ESG shared task.")
parser.add_argument("-tc", "--target_cols", nargs="+",
                    help="Which column of the dataset to use as target, if not all. Use col numbers: 0 4, 0 1 2, ...")
parser.add_argument("-tw", "--task_weighting", action="store_true", default=False,
                help="Boolean: If True, learn weights for specific tasks, only applicable when multitask is true")
parser.add_argument("-seq", "--sequential_training", action="store_true", default=False,
                help="Boolean: fine-tune on each task seprately after training on several other tasks (use with -mt flag).")
parser.add_argument("-tce", "--targetcol_explicit", type=int, default=None,
                help="Target column for 'explicit' multi-task training (adding other tasks logits as features). Should the nb of the column.")


if __name__ == "__main__":
    args = parser.parse_args()
    
    batch_size = 64
    
    features = {'part1_new': ['Relevance[business related text / general text]',\
       'Financial sentiment[positive/neutral/negative]',\
       'Objectivity[objective/subjective]', 'Forward looking[FLS/non-FLS]',\
       'ESG[ESG related/not ESG related]'], \
        'part2_new': ['ESG/not-Esg', 'ESG type']}
    
    task_dict = {'Relevance[business related text / general text]': 2,\
       'Financial sentiment[positive/neutral/negative]':3,\
       'Objectivity[objective/subjective]':2, 'Forward looking[FLS/non-FLS]':2,\
       'ESG[ESG related/not ESG related]':2}
    
    task_nums = {'Relevance': 0, 'Financial_sentiment': 1, 'Objectivity': 2, 'Forward_looking': 3, 'ESG': 4}

    
    tokenizer = AutoTokenizer.from_pretrained(args.model_path)
        
    # best seeds for each EXPLICIT MLT model, from aggregate_results.ipynb
    best_seeds = {'Relevance': 'seed_2',
        'Financial_sentiment': 'seed_4',
        'ESG': 'seed_4',
        'Forward_looking': 'seed_4',
        'Objectivity': 'seed_2'}
    best_models = {}
    for task, seed in best_seeds.items():
        tasknum = task_nums[task]
        bestseed = best_seeds[task]
        path = f'models/roberta-ftse/final_EXPL/target_{tasknum}/{bestseed}/checkpoint-710/'
        model = RobertaForSequenceMultiClassification.from_pretrained(path, task_dict, device, args, features, predict_only = True)
        model = model.to(device)
        best_models[task] = model
        
    df_res = pd.DataFrame(columns=['ticker', 'year', 'nb_sentences'] + list(task_dict.keys()))
    all_res_dict = {}
    
    df_res = pd.DataFrame(columns=['Sentence'] + list(task_dict.keys()))

    companies_paths = sorted(os.listdir(args.input))
    for ticker in tqdm(companies_paths):
        output_path = 'data/ftse350_corpus_predictions/' + ticker
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        for txtfile in glob.glob(args.input + ticker + '/*.txt'):
            if os.path.exists(txtfile.replace('.txt', '.tsv').replace('_preprocessed', '_predictions')):
                continue
            file_res_dict = {}
            year = txtfile.split('_')[-1].replace('.txt','')
            all_res_dict['ticker'] = ticker
            all_res_dict['year'] = year
            with open(txtfile, encoding="utf8") as input:
                sentences = input.readlines()
            # Deduplicate
            all_res_dict['nb_sentences'] = len(sentences)
            preds_dict = {}
            # initialize the dict
            preds_dict['sentence'] = []
            for i, task in enumerate(task_dict.keys()):
                preds_dict[task] = []
            # fill it for all tasks at each batch
            batches = [sentences[n:n+batch_size] for n in range(0, len(sentences), batch_size)]
            for batch_sentences in batches:
                encodings = tokenizer(batch_sentences, truncation=True, padding="max_length", max_length = 128, return_tensors='pt').to(device)
                preds_dict['sentence'].extend(batch_sentences)
                for i, task in enumerate(task_dict.keys()):
                    taskname = task.split('[')[0].replace(' ', '_')
                    with torch.no_grad():
                        logits = best_models[taskname](**encodings)['logits']
                    for row in logits[i]: # for each example
                        row = row.cpu().detach().numpy()
                        if len(row)==1:
                            preds_dict[task].append(int(row>0.5))
                        else:
                            preds_dict[task].append(int(np.argmax(row)))            
            pd.DataFrame(preds_dict).to_csv(output_path + '/' + os.path.basename(txtfile).replace('.txt', '.tsv'), sep='\t')
                
                
                
                
                
    
    
